# -*- coding: utf-8 -*-

from collections import deque
import copy

class GenerateBoard:
    def __init__(self, parent_board):
        self.all_boards = [[]]
        self.all_boards.append(parent_board)  
    
    def gen_all_board(self, player, index):
        board = self.all_boards[index]
        print('parent board: ')
        for i in range(0,3):
            print(board[str(i) + '0'], board[str(i) + '1'], board[str(i) + '2'])
        print('###################')
        for key, value in board.items():
            child_board = copy.deepcopy(board)    
            if board[key] == '-':
                child_board[key] = 'O' if player == 'computer' else 'X'
                for i in range(0,3):
                    print(child_board[str(i) + '0'], child_board[str(i) + '1'], child_board[str(i) + '2'])
                child_board['parent'] = index
                print('###################')    
                self.all_boards.append(child_board)
                
  
starting_character = input('who starts: ')
num_of_empty_space = 8
for i in range(0,40320):
  if num_of_empty_space %2 == 0:
    player = 'computer'
  else:
    player = 'human' 
  print(player)
  
#each time the generate function is called the following params should be given: source board, character
        
                
                