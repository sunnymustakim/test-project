# -*- coding: utf-8 -*-
import random
from collections import Counter
import board_state 
class Board:
    board = {'00': '-', '01': '-', '02': '-', '10': '-', '11': '-', '12': '-', '20': '-', '21': '-', '22': '-', 'parent':None,'state':None} 
    all_cells = ['00', '01', '02', '10', '11', '12', '20', '21', '22'] 
    
    def set_value(self, position, player):
        #print(type(self.board[position]))
        if position in self.all_cells and self.board[position] == '-': 
            self.board[position] = player.sign
            self.all_cells.remove(position)
        else:
            print('Not a valid entry!')
    
    def draw_board(self):
        for i in range(0,3):
            print(self.board[str(i) + '0'], self.board[str(i) + '1'], self.board[str(i) + '2'])
       
    
    def defined_rules(self, board):
        if self.board['00'] == self.board['01'] == 'X' and self.board['02'] == '-':
            return '02'
        if self.board['00'] == self.board['02'] == 'X' and self.board['01'] == '-':
            return '01'
        if self.board['01'] == self.board['02'] == 'X' and self.board['00'] == '-':
            return '00'
        if self.board['10'] == self.board['11'] == 'X' and self.board['12'] == '-':
            return '12'
        if self.board['10'] == self.board['12'] == 'X' and self.board['11'] == '-':
            return '11'
        if self.board['11'] == self.board['12'] == 'X' and self.board['10'] == '-':
            return '10'
        if self.board['20'] == self.board['21'] == 'X' and self.board['22'] == '-':
            return '22'
        if self.board['20'] == self.board['22'] == 'X' and self.board['21'] == '-':
            return '21'
        if self.board['21'] == self.board['22'] == 'X' and self.board['20'] == '-':
            return '20'
        if self.board['00'] == self.board['10'] == 'X' and self.board['20'] == '-':
            return '20'
        if self.board['00'] == self.board['20'] == 'X' and self.board['10'] == '-':
            return '10'
        if self.board['10'] == self.board['20'] == 'X' and self.board['00'] == '-':
            return '00'
        if self.board['01'] == self.board['11'] == 'X' and self.board['21'] == '-':
            return '21'
        if self.board['01'] == self.board['21'] == 'X' and self.board['11'] == '-':
            return '11'
        if self.board['11'] == self.board['21'] == 'X' and self.board['01'] == '-':
            return '01'
        if self.board['02'] == self.board['12'] == 'X' and self.board['22'] == '-': 
            return '22'
        if self.board['02'] == self.board['22'] == 'X' and self.board['12'] == '-':
            return '12'
        if self.board['12'] == self.board['22'] == 'X' and self.board['02'] == '-':
            return '02'
        if self.board['00'] == self.board['11'] == 'X' and self.board['22'] == '-': 
            return '22'
        if self.board['00'] == self.board['22'] == 'X' and self.board['11'] == '-':
            return '11'
        if self.board['11'] == self.board['22'] == 'X' and self.board['00'] == '-':
            return '00'
        if self.board['20'] == self.board['11'] == 'X' and self.board['02'] == '-': 
            return '02'
        if self.board['20'] == self.board['02'] == 'X' and self.board['11'] == '-':
            return '11'
        if self.board['11'] == self.board['02'] == 'X' and self.board['20'] == '-':
            return '20'
        else:
            return random.choice(self.all_cells)
            
        
            

class Player: 
    def __init__(self, name, sign):
        self.name = name
        self.sign = sign
    
    
       
sunny = Player('Sunny', 'X')
computer = Player('Computer', 'O')
b = Board()
b.draw_board()

player = sunny
while True:
    if Counter(b.board.values())['-'] == 0:
        print('match drawn')
        break
    if player == sunny:
        b.set_value(input('Please enter poistion: '), player)
        #print('Value of - : ', Counter(b.board.values())['-'])
        b.draw_board()
        if Counter(b.board.values())['-'] <= 4:
            result = board_state.find_state(b.board)
            #result = b.decision(player)
            #if result == False:
            if result == 1 or result == -1:
                print(player.name, ' Won!')
                break
        
        player = computer
        print('\n')
    else:
        print('computers turn')
        if Counter(b.board.values())['-'] >= 6:
            #print('remaining places for computer to play:', Counter(b.board.values())['-'])
            position = b.defined_rules(b.board)
            print('Position from non AI: ', position)
            #b.set_value(random.choice(b.all_cells), player)
            b.set_value(b.defined_rules(b.board),player)
            #print('Value of - : ', Counter(b.board.values())['-'])
            b.draw_board()
            if Counter(b.board.values())['-'] <= 4:
                result = board_state.find_state(b.board)
                #result = b.decision(player)
                #if result == False:
                if result == 1 or result == -1:
                    print(player.name, ' Won!')
                    break
            
            player = sunny
            print('\n')
        else:
            board_state.all_boards = []
            print('time to swtich to intelligent mode')
            #print('drawing board before sending it to AI')
            #b.draw_board()
            #print(b.board)
            source_board = b.board
            board_state.all_boards.append(source_board)
            board_state.gen_all_board(source_board,0)
            
            board = board_state.optimal_move(board_state.all_boards,source_board)
            b.board = board 
            b.draw_board()
            if Counter(b.board.values())['-'] <= 4:
                result = board_state.find_state(b.board)
                #result = b.decision(player)
                #if result == False:
                if result == 1 or result == -1:
                    print(player.name, ' Won!')
                    break
            
            player = sunny
            print('\n')
            #break
      
