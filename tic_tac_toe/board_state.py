# -*- coding: utf-8 -*-

import copy
from collections import Counter
all_boards = []
def gen_all_board(source_board,counter):
    
    score = find_state(source_board)
    if score == 1:
        source_board['state'] = 1
    elif score == -1:
        source_board['state'] = -1
        
        parent = source_board['parent']
        while parent != 0:
            all_boards[parent]['state'] = -1
            parent = all_boards[parent]['parent']
        
    else:
        for key, value in source_board.items():
            copy_source_board = copy.deepcopy(source_board)
            if source_board[key] == '-':
                copy_source_board[key] = 'O' if Counter(source_board.values())['-']%2 == 0 else 'X'
                copy_source_board['parent'] = all_boards.index(source_board)
                copy_source_board['state'] = 'undecided'
                all_boards.append(copy_source_board)
    
    
    counter += 1
    if counter < len(all_boards):
        gen_all_board(all_boards[counter],counter)
        
    
def optimal_move(all_boards,source_board):
    depth_one_boards = []
    for board in all_boards:
        if Counter(board.values())['-'] == Counter(source_board.values())['-'] - 1:
            depth_one_boards.append(board)
    
    for board in depth_one_boards:
        if board['state'] == 1:
            print('win')
            return board
    
    for board in depth_one_boards:
        if board['state'] == 'undecided':
            print('undecided')
            return board
    
    
    reversed_boards = copy.deepcopy(all_boards) 
    reversed_boards.reverse()
    for board in reversed_boards:
        if Counter(board.values())['-'] == 0 and board['state'] == 'undecided':
            parent = board['parent']
            break
            
        else:
            for board in all_boards:
                if board['state'] == 'undecided':
                    parent = board['parent']
                    break
                if board['state'] == 1:
                    parent = board['parent']
                    break
                
    
    while parent != 0:
        board = all_boards[parent]
        parent = all_boards[parent]['parent']
        
    return board
        
       

def find_state(board):
    if board['00'] == board['01'] == board['02'] == 'O': 
        return 1
    if board['00'] == board['01'] == board['02'] == 'X':
        return -1
    if board['00'] == board['10'] == board['20'] == 'O':  
        return 1
    if board['00'] == board['10'] == board['20'] == 'X':
        return -1
    if board['20'] == board['21'] == board['22'] == 'O':  
        return 1
    if board['20'] == board['21'] == board['22'] == 'X':
        return -1
    if board['22'] == board['12'] == board['02'] == 'O':  
        return 1
    if board['22'] == board['12'] == board['02'] == 'X':
        return -1
    if board['00'] == board['11'] == board['22'] == 'O':  
        return 1
    if board['00'] == board['11'] == board['22'] == 'X':
        return -1
    if board['20'] == board['11'] == board['02'] == 'O':  
        return 1
    if board['20'] == board['11'] == board['02'] == 'X':
        return -1
    if board['10'] == board['11'] == board['12'] == 'O':  
        return 1
    if board['10'] == board['11'] == board['12'] == 'X':
        return -1
    if board['01'] == board['11'] == board['21'] == 'O':  
        return 1
    if board['01'] == board['11'] == board['21'] == 'X':
        return -1 
    
